<?php
// source: D:\PhpDev\EasyPHP-Devserver-16.1\eds-www\znfcv03b\app\presenters/templates/Pid/default.latte

use Latte\Runtime as LR;

class Templatec9c535c7df extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['pid'])) trigger_error('Variable $pid overwritten in foreach on line 20');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<h1>Rodná čísla</h1>

<p>
<p>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Company:default")) ?>">Firmy</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Employer:default")) ?>">Zaměstanci</a>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistiky</a></p>
    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a>
</p>

<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
<table border="1" width="100%">
    <tr>
        <th>Rodné číslo</th>
        <th colspan="2">Akce</th>

    </tr>
<?php
		$iterations = 0;
		foreach ($pids as $pid) {
?>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($pid['name']) /* line 22 */ ?></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $pid['id']])) ?>">Edituj</a></td>
                <td><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $pid['id']])) ?>">Odeber</a></td>
            </tr>
<?php
			$iterations++;
		}
?>
    </div>
</table><?php
	}

}
