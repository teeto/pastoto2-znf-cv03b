<?php
// source: D:\PhpDev\EasyPHP-Devserver-16.1\eds-www\znfcv03b\app\presenters/templates/Employer/default.latte

use Latte\Runtime as LR;

class Templatefaea1194ff extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
		'css' => 'blockCss',
	];

	public $blockTypes = [
		'content' => 'html',
		'css' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
?>

<?php
		$this->renderBlock('css', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['e'])) trigger_error('Variable $e overwritten in foreach on line 25');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

    <h1>Zaměstnanci</h1>

    <p>
        <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a>
        <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Company:default")) ?>">Firmy</a>
        <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistiky</a>
        <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a>
    </p>

    <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
    <div class="table">
        <div class="row">
            <div class="cell tencol">Firma</div>
            <div class="cell tencol">Jméno</div>
            <div class="cell tencol">Příjmení</div>
            <div class="cell tencol">Rodné číslo</div>
            <div class="cell tencol">Pohlaví</div>
            <div class="cell tencol">Datum narození</div>
            <div class="cell tencol">Plat</div>
            <div class="cell tencol">Daň</div>
            <div class="cell tencol">Akce</div>
        </div>
<?php
		$iterations = 0;
		foreach ($employers as $e) {
?>
            <div class="row">
                <div class="cell tencol"><?php echo LR\Filters::escapeHtmlText($e->company->name) /* line 27 */ ?> </div>
                <div class="cell tencol"><a href="http://www.kdejsme.cz/prijmeni/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->surname)) /* line 28 */ ?>/"><?php
			echo LR\Filters::escapeHtmlText($e->surname) /* line 28 */ ?></a></div>
                <div class="cell tencol"><a href="http://www.kdejsme.cz/prijmeni/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->firstname)) /* line 29 */ ?>/"><?php
			echo LR\Filters::escapeHtmlText($e->firstname) /* line 29 */ ?></a></div>
                <div class="cell tencol"><?php
			if (isset($e->pid_id)) {
				echo LR\Filters::escapeHtmlText($e->pid->name) /* line 30 */;
			}
?></div>
                <div class="cell tencol"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->sex, $e->pid_id)) /* line 31 */ ?></div>
                <div class="cell tencol"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->birthday, $e->pid_id)) /* line 32 */ ?></div>
                <div class="cell tencol"><?php echo LR\Filters::escapeHtmlText($e->salary) /* line 33 */ ?></div>
                <div class="cell tencol"><?php echo LR\Filters::escapeHtmlText($e->salary * 0.22) /* line 34 */ ?></div>
                <div class="cell tencol"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $e->id])) ?>">Edituj</a> | <a href="<?php
			echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $e->id])) ?>">Odeber</a></div>
            </div>
<?php
			$iterations++;
		}
?>
    </div>

<?php
	}


	function blockCss($_args)
	{
		extract($_args);
?>
    <style>
        .table {
            width: 100%;
            height: 100%;
        }

        .row {
            width: 100%;
            min-height: 1px;
            height: auto;
            margin: 0;
        }

        .cell {
            float: left;
            margin: 0;
            padding: 0;
        }

        .onecol {
            width: 100%;
        }

        .twocol {
            width: 50%;
        }

        .tencol {
            width: 11%;
        }
    </style>
<?php
	}

}
