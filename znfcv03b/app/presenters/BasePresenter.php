<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;

/**
 * Class BasePresenter
 * @package App\Presenters
 */
abstract class BasePresenter extends Presenter{

    protected function beforeRender()
    {
        $this->template->addFilter('phone', function ($number) {
            if(preg_match('/^[0-9]{9}$/',$number)){
                return "+420 ".substr($number,-9,3)." ".substr($number,-6,3)." ".substr($number,-3,3)."";
            }
            return "!!";
        });
    }
}
