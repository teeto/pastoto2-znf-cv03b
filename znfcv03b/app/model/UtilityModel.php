<?php

namespace App\Model;

use App\Model\PidModel;
use Nette;

class UtilityModel extends BaseModel
{
    /** @var PidModel - model pro management rc*/
    private $pidModel;

    public function __construct(Nette\Database\Context $database, PidModel $pidModel)
    {
        parent::__construct($database);
        $this->pidModel=$pidModel;
    }

    /*public function injectDependencies(
        PidModel $pidModel
    )
    {
        $this->pidModel = $pidModel;
    }*/

    /**
     * Metoda detekuje pohlaví -1 = nedefinováno, 0 - žena, 1 - muž
     * @param int  $id rodného čísla
     */
    public function isMan($id)
    {
        if(!$id) return -1;
        $pid = $this->pidModel->getPid($id);
        if(!$pid) return -1;
        $rc = substr($pid['name'],2,2);
        return $rc<50;
    }

    /**
     * Metoda detekuje datum narození
     * @param int  $id rodného čísla
     */
    public function getBirthDay($id)
    {
        if(!$id) return -1;
        $pid = $this->pidModel->getPid($id)['name'];
        $year = substr($pid,0,2);
        $month = substr($pid,2,2);
        if(!$this->isMan($id))
            $month-=50;
        $day = substr($pid,4,2);
        return $day.".".$month.".".$year;
    }

}